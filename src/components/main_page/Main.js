import React from "react";

/**
 * This is a class representing the main page of the application. 
 */
class Main extends React.Component {

  render() {

    return (
      <>
        <nav>
          <div class="nav-left">
            <a href="Index.html">
              <img src="https://i.postimg.cc/Y9nZymQq/logo2.png" class="logo"alt=""/>
            </a>
            <ul>
              <li><img src="https://i.postimg.cc/Fs3m1Djy/notification.png" alt=""/></li>
              <li><img src="https://i.postimg.cc/YqGKZ8nc/inbox.png" alt=""/></li>
              <li><img src="https://i.postimg.cc/xCzpgFjg/video.png" alt=""/></li>
            </ul>
          </div>
          <div class="nav-right">
            <div class="search-box">
              <img src="https://i.postimg.cc/SKtHgM6Q/search.png" alt=""/>
                <input type="text" placeholder="Search" alt=""/>
                </div>
                <div class="nav-user-icon online" onclick="settingsMenuToggle()">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                </div>
            </div>
            {/* <!----------------Settings Menu"-----------------------> */}
            <div class="settings-menu">
              <div id="dark-btn">
                <span></span>
              </div>
              <div class="settings-menu-inner">
                <div class="user-profile">
                  <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                    <div>
                      <p>John Nicholson</p>
                      <a href="profile.html">See your profile</a>
                    </div>
                </div>
                <hr alt=""/>
                  <div class="user-profile">
                    <img src="https://i.postimg.cc/hv3nx52s/feedback.png" alt=""/>
                      <div>
                        <p>Give Feedback</p>
                        <a href="#">Help us to improve the new design</a>
                      </div>
                  </div>
                  <hr/>
                    <div class="settings-links">
                      <img src="https://i.postimg.cc/QCcPNYRV/setting.png" class="settings-icon" alt=""/>
                        <a href="#">Settings & Privacy <img src="https://i.postimg.cc/RF1dBMWr/arrow.png" width="10px" alt=""/></a>
                    </div>
                    <div class="settings-links">
                      <img src="https://i.postimg.cc/C5tydfK6/help.png" class="settings-icon" alt=""/>
                        <a href="#">Help & Support<img src="https://i.postimg.cc/RF1dBMWr/arrow.png" width="10px" alt=""/></a>
                    </div>
                    <div class="settings-links">
                      <img src="https://i.postimg.cc/5yt1XVSj/display.png" class="settings-icon" alt=""/>
                        <a href="#">Display & Accessibility <img src="https://i.postimg.cc/RF1dBMWr/arrow.png" width="10px"alt=""/></a>
                    </div>
                    <div class="settings-links">
                      <img src="https://i.postimg.cc/PJC9GrMb/logout.png" class="settings-icon" alt=""/>
                        <a href="#">Logout <img src="https://i.postimg.cc/RF1dBMWr/arrow.png" width="10px"alt=""/></a>
                    </div>
                  </div>
              </div>
            </nav>
            <div class="container">
              {/* <!----------------Left Sidebar-----------------------> */}
              <div class="left-sidebar">
                <div class="imp-links">
                  <a href="#"><img src="https://i.postimg.cc/RCj4MjnC/news.png" alt=""/>Latest News</a>
                  <a href="#"><img src="https://i.postimg.cc/MpBwMtV0/friends.png" alt=""/>Friendss</a>
                  <a href="#"><img src="https://i.postimg.cc/44FRWj1b/group.png" alt=""/>Group</a>
                  <a href="#"><img src="https://i.postimg.cc/0jh39RtT/marketplace.png" alt=""/>Marketplace</a>
                  <a href="#"><img src="https://i.postimg.cc/VsXHCTVk/watch.png" alt=""/>Watch</a>
                  <a href="#">See More</a>
                </div>
                <div class="shortcut-link">
                  <p>Your Shortcuts</p>
                  <a href="#"><img src="https://i.postimg.cc/3JHVf7vG/shortcut-1.png" alt=""/>Web Developers</a>
                  <a href="#"><img src="https://i.postimg.cc/rFCbvb1P/shortcut-2.png" alt=""/>Web Design course</a>
                  <a href="#"><img src="https://i.postimg.cc/0yk3xfZ2/shortcut-3.png" alt=""/>Full Strack Development</a>
                  <a href="#"><img src="https://i.postimg.cc/Z5wQqdDP/shortcut-4.png" alt=""/>Website Experts</a>
                </div>
              </div>
              {/* <!----------------Main Sidebar-----------------------> */}
              <div class="main-content">
                <div class="story-gallery">
                  <div class="story story1">
                    <img src="https://i.postimg.cc/TPh453Zz/upload.png" alt=""/>
                      <p>Post Story</p>
                  </div>
                  <div class="story story2">
                    <img src="https://i.postimg.cc/XNPtfdVs/member-1.png" alt=""/>
                      <p>Alison</p>
                  </div>
                  <div class="story story3">
                    <img src="https://i.postimg.cc/4NhqByys/member-2.png" alt=""/>
                      <p>Jackson</p>
                  </div>
                  <div class="story story4">
                    <img src="https://i.postimg.cc/FH5qqvkc/member-3.png" alt=""/>
                      <p>Samona</p>
                  </div>
                  <div class="story story5">
                    <img src="https://i.postimg.cc/Sx65bPcP/member-4.png" alt=""/>
                      <p>John Doe</p>
                  </div>
                </div>
                <div class="write-post-container">
                  <div class="user-profile">
                    <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                      <div>
                        <p>John Nicholson</p>
                        <small>Public <i class="fas fa-caret-down"></i></small>
                      </div>
                  </div>
                  <div class="post-input-container">
                    <textarea rows="3" placeholder="What's on your mind, John?"></textarea>
                    <div class="add-post-links">
                      <a href="#"><img src="https://i.postimg.cc/QMD2BDXs/live-video.png" alt=""/>Live Video</a>
                      <a href="#"><img src="https://i.postimg.cc/6pKKZn0D/photo.png" alt=""/>Photo/Video</a>
                      <a href="#"><img src="https://i.postimg.cc/Pf6TBCdD/feeling.png" alt=""/>Feling/Activity</a>
                    </div>
                  </div>
                </div>
                <div class="post-container">
                  <dic class="post-row">
                    <div class="user-profile">
                      <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                        <div>
                          <p>John Nicholson</p>
                          <span>June 24 2021, 13:40 pm</span>
                        </div>
                    </div>
                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                  </dic>
                  <p class="post-text">Subscribe <span>@Vkive Tutorials</span> Youtube Channel to watch more videos on
                    website development and UI desings. <a href="#">#VkiveTutorials</a> <a href="#">#YoutubeChannel</a></p>
                  <img src="https://i.postimg.cc/9fjhGTY6/feed-image-1.png" class="post-img" alt=""/>
                    <div class="post-row">
                      <div class="activity-icons">
                        <div><img src="https://i.postimg.cc/pLKNXrMq/like-blue.png"  alt=""/>120</div>
                        <div><img src="https://i.postimg.cc/rmjMymWv/comments.png"  alt=""/>45</div>
                        <div><img src="https://i.postimg.cc/T2bBchpG/share.png"  alt=""/>20</div>
                      </div>
                      <div class="post-porfile-icon">
                        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png"  alt=""/><i class="fas fa-caret-down"></i>
                      </div>
                    </div>
                </div>
                <div class="post-container">
                  <dic class="post-row">
                    <div class="user-profile">
                      <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png"  alt=""/>
                        <div>
                          <p>John Nicholson</p>
                          <span>June 24 2021, 13:40 pm</span>
                        </div>
                    </div>
                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                  </dic>
                  <p class="post-text">Like and share this video with friends, tag<span>@Vkive Tutorials</span>facebook page on your post.
                    Ask your dobuts in the comments. <a href="#">#VkiveTutorials</a> <a href="#">#YoutubeChannel</a></p>
                  <img src="https://i.postimg.cc/Xvc0xJ2p/feed-image-2.png" class="post-img"  alt=""/>
                    <div class="post-row">
                      <div class="activity-icons">
                        <div><img src="https://i.postimg.cc/pLKNXrMq/like-blue.png"  alt=""/>120</div>
                        <div><img src="https://i.postimg.cc/rmjMymWv/comments.png"  alt=""/>45</div>
                        <div><img src="https://i.postimg.cc/T2bBchpG/share.png"  alt=""/>20</div>
                      </div>
                      <div class="post-porfile-icon">
                        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png"  alt=""/><i class="fas fa-caret-down"></i>
                      </div>
                    </div>
                </div>
                <div class="post-container">
                  <dic class="post-row">
                    <div class="user-profile">
                      <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png"  alt=""/>
                        <div>
                          <p>John Nicholson</p>
                          <span>June 24 2021, 13:40 pm</span>
                        </div>
                    </div>
                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                  </dic>
                  <p class="post-text">Like and share this video with friends, tag<span>@Vkive Tutorials</span>facebook page on your post.
                    Ask your dobuts in the comments. <a href="#">#VkiveTutorials</a> <a href="#">#YoutubeChannel</a></p>
                  <img src="https://i.postimg.cc/tJ7QXz9x/feed-image-3.png" class="post-img"  alt=""/>
                    <div class="post-row">
                      <div class="activity-icons">
                        <div><img src="https://i.postimg.cc/pLKNXrMq/like-blue.png"  alt=""/>120</div>
                        <div><img src="https://i.postimg.cc/rmjMymWv/comments.png"  alt=""/>45</div>
                        <div><img src="https://i.postimg.cc/T2bBchpG/share.png"  alt=""/>20</div>
                      </div>
                      <div class="post-porfile-icon">
                        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/><i class="fas fa-caret-down"></i>
                      </div>
                    </div>
                </div>
                <div class="post-container">
                  <dic class="post-row">
                    <div class="user-profile">
                      <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                        <div>
                          <p>John Nicholson</p>
                          <span>June 24 2021, 13:40 pm</span>
                        </div>
                    </div>
                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                  </dic>
                  <p class="post-text">Like and share this video with friends, tag<span>@Vkive Tutorials</span>facebook page on your post.
                    Ask your dobuts in the comments. <a href="#">#VkiveTutorials</a> <a href="#">#YoutubeChannel</a></p>
                  <img src="https://i.postimg.cc/hjDRYBwM/feed-image-4.png" class="post-img" alt=""/>
                    <div class="post-row">
                      <div class="activity-icons">
                        <div><img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt=""/>120</div>
                        <div><img src="https://i.postimg.cc/rmjMymWv/comments.png" alt=""/>45</div>
                        <div><img src="https://i.postimg.cc/T2bBchpG/share.png" alt=""/>20</div>
                      </div>
                      <div class="post-porfile-icon">
                        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/><i class="fas fa-caret-down"></i>
                      </div>
                    </div>
                </div>
                <div class="post-container">
                  <dic class="post-row">
                    <div class="user-profile">
                      <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/>
                        <div>
                          <p>John Nicholson</p>
                          <span>June 24 2021, 13:40 pm</span>
                        </div>
                    </div>
                    <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                  </dic>
                  <p class="post-text">Like and share this video with friends, tag<span>@Vkive Tutorials</span>facebook page on your post.
                    Ask your dobuts in the comments. <a href="#">#VkiveTutorials</a> <a href="#">#YoutubeChannel</a></p>
                  <img src="https://i.postimg.cc/ZRwztQzm/feed-image-5.png" class="post-img" alt=""/>
                    <div class="post-row">
                      <div class="activity-icons">
                        <div><img src="https://i.postimg.cc/pLKNXrMq/like-blue.png" alt=""/>120</div>
                        <div><img src="https://i.postimg.cc/rmjMymWv/comments.png" alt=""/>45</div>
                        <div><img src="https://i.postimg.cc/T2bBchpG/share.png" alt=""/>20</div>
                      </div>
                      <div class="post-porfile-icon">
                        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt=""/><i class="fas fa-caret-down"></i>
                      </div>
                    </div>
                </div>
                <button type="button" class="load-more-btn">Load More</button>
              </div>
              {/* <!----------------Right Sidebar-----------------------> */}
              <div class="right-sidebar">
                <div class="sidebar-title">
                  <h4>Events</h4>
                  <a href="#">See All</a>
                </div>
                <div class="event">
                  <div class="left-event">
                    <h3>18</h3>
                    <span>March</span>
                  </div>
                  <div class="right-event">
                    <h4>Social Media</h4>
                    <p><i class="fas fa-map-marker-alt"></i> Willson Tech Park</p>
                    <a href="#">More Info</a>
                  </div>
                </div>
                <div class="event">
                  <div class="left-event">
                    <h3>22</h3>
                    <span>June</span>
                  </div>
                  <div class="right-event">
                    <h4>Mobile Marketing</h4>
                    <p><i class="fas fa-map-marker-alt"></i> Willson Tech Park</p>
                    <a href="#">More Info</a>
                  </div>
                </div>
                <div class="sidebar-title">
                  <h4>Advertisement</h4>
                  <a href="#">close</a>
                </div>
                <img src="https://i.postimg.cc/CLXYx9BL/advertisement.png" class="siderbar-ads" alt=""/>
                  <div class="sidebar-title">
                    <h4>Conversation</h4>
                    <a href="#">Hide Chat</a>
                  </div>
                  <div class="online-list">
                    <div class="online">
                      <img src="https://i.postimg.cc/XNPtfdVs/member-1.png" alt=""/>
                    </div>
                    <p>Alison Mina</p>
                  </div>
                  <div class="online-list">
                    <div class="online">
                      <img src="https://i.postimg.cc/4NhqByys/member-2.png" alt=""/>
                    </div>
                    <p>Jackson Aston</p>
                  </div>
                  <div class="online-list">
                    <div class="online">
                      <img src="https://i.postimg.cc/FH5qqvkc/member-3.png" alt=""/>
                    </div>
                    <p>Samona Rose</p>
                  </div>
                  <div class="online-list">
                    <div class="online">
                      <img src="https://i.postimg.cc/Sx65bPcP/member-4.png" alt=""/>
                    </div>
                    <p>Mike Pérez</p>
                  </div>
              </div>
            </div>
            <div class="footer">
              <p>Copyright 2022 - Vkive Tutorials</p>
            </div>
         </>
          )
  }
  
}

          export default Main