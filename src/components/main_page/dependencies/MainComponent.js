const StoryContainer = () => {
  var shortcuts = [
    {
      link: "https://i.postimg.cc/TPh453Zz/upload.png",
      name: "Post story",
      class: "story story1",
    },
    {
      link: "https://i.postimg.cc/XNPtfdVs/member-1.png",
      name: "Alison",
      class: "story story2",
    },
    {
      link: "https://i.postimg.cc/4NhqByys/member-2.png",
      name: "Jackson",
      class: "story story3",
    },
    {
      link: "https://i.postimg.cc/FH5qqvkc/member-3.png",
      name: "Samona",
      class: "story story4",
    },
    {
      link: "https://i.postimg.cc/Sx65bPcP/member-4.png",
      name: "John Doe",
      class: "story story5",
    },
  ];
  return (
    <div className="story-gallery">
      {shortcuts.map((story) => {
        return (
          <div className={story.class}>
            <img src={story.link} alt={story.name} />
            <p>{story.name}</p>
          </div>
        );
      })}
    </div>
  );
};

const WritePostContainer = () => {
  return (
    <div className="write-post-container">
      <div className="user-profile">
        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="" />
        <div>
          <p>John Nicholson</p>
          <small>
            Public <i className="fas fa-caret-down"></i>
          </small>
        </div>
      </div>
      <div className="post-input-container">
        <textarea rows="3" placeholder="What's on your mind, John?"></textarea>
        <div className="add-post-links">
          <a href="/">
            <img src="https://i.postimg.cc/QMD2BDXs/live-video.png" alt="" />
            Live Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/6pKKZn0D/photo.png" alt="" />
            Photo/Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/Pf6TBCdD/feeling.png" alt="" />
            Feling/Activity
          </a>
        </div>
      </div>
    </div>
  );
};
const PostContainer = () => {
  var shortcut = [
    {
      postdesc:
        "Subscribe @Vkive Tutorials Youtube Channel to watch more videos on website development and UI desings",
      postImg: "https://i.postimg.cc/9fjhGTY6/feed-image-1.png",
    },
    {
      postdesc:"Like and share this video with friends, tag @Vkive Tutorials facebook page on your post. Ask your dobuts in the comments.",
      postImg:"https://i.postimg.cc/Xvc0xJ2p/feed-image-2.png",
    },
    {
      postdesc:"Like and share this video with friends, tag @Vkive Tutorials facebook page on your post. Ask your dobuts in the comments.",
      postImg:"https://i.postimg.cc/tJ7QXz9x/feed-image-3.png",
    },
    {
      postdesc:"Like and share this video with friends, tag @Vkive Tutorials facebook page on your post. Ask your dobuts in the comments.",
      postImg:"https://i.postimg.cc/hjDRYBwM/feed-image-4.png",
    },
    {
      postdesc:"Like and share this video with friends, tag @Vkive Tutorials facebook page on your post. Ask your dobuts in the comments.",
      postImg:"https://i.postimg.cc/ZRwztQzm/feed-image-5.png",
    }
   
  ];
  return (
    <>
      {shortcut.map((post) => {
        return (
          <div  className="post-container">
            <div className="post-row">
              <div className="user-profile">
                <img
                  src="https://i.postimg.cc/cHg22LhR/profile-pic.png"
                  alt=""
                />
                <div>
                  <p>John Nicholson</p>
                  <span>June 24 2021, 13:40 pm</span>
                </div>
              </div>
              <a href="/">
                <i className="fas fa-ellipsis-v"></i>
              </a>
            </div>
            <p className="post-text">
             {post.postdesc}
              <a href="/">#VkiveTutorials</a> <a href="/">#YoutubeChannel</a>
            </p>
            <img
              src={post.postImg}
              className="post-img"
              alt=""
            />
            <div className="post-row">
              <div className="activity-icons">
                <div>
                  <img
                    src="https://i.postimg.cc/pLKNXrMq/like-blue.png"
                    alt=""
                  />
                  120
                </div>
                <div>
                  <img
                    src="https://i.postimg.cc/rmjMymWv/comments.png"
                    alt=""
                  />
                  45
                </div>
                <div>
                  <img src="https://i.postimg.cc/T2bBchpG/share.png" alt="" />
                  20
                </div>
              </div>
              <div className="post-porfile-icon">
                <img
                  src="https://i.postimg.cc/cHg22LhR/profile-pic.png"
                  alt=""
                />
                <i className="fas fa-caret-down"></i>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

const MainComponent = () => {
  return (
    <div className="main-content">
      <StoryContainer />
      <WritePostContainer />
      <PostContainer/>
      
      <button type="button" className="load-more-btn">
        Load More
      </button>
    </div>
  );
};

export default MainComponent;
