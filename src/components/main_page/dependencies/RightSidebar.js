const RightSidebar = () => {
  return (
    <div className="right-sidebar">
     <Events/>
     <Advertisement/>
      <Conversations />
    </div>
  );
};
const Events= ()=>{
  var shortcut= [
    {date:"18",month:"March",event:"Social Media",location:'Willson Tech Park'},
    {date:"22",month:"June",event:"Mobile Marketting",location:'Willson Tech Park'}

  ]
  return(
    <>
     <div className="sidebar-title">
        <h4>Events</h4>
        <a href="/">See All</a>
      </div>
      {shortcut.map(data=>{
        return(
          <div className="event">
        <div className="left-event">
          <h3>{data.date}</h3>
          <span>{data.month}</span>
        </div>
        <div className="right-event">
          <h4>{data.event}</h4>
          <p>
            <i className="fas fa-map-marker-alt"></i>{data.location}
          </p>
          <a href="/">More Info</a>
        </div>
      </div>
        )
      }

      )}
      
      </>
  )
}
const Advertisement = () =>{
  return(<>
   <div className="sidebar-title">
        <h4>Advertisement</h4>
        <a href="/">close</a>
      </div>
      <img
        src="https://i.postimg.cc/CLXYx9BL/advertisement.png"
        className="siderbar-ads"
        alt=""
      /></>)
  
}
const Conversations = () => {
  var shortcut = [
    {
      imglink: "https://i.postimg.cc/XNPtfdVs/member-1.png",
      username: "Alison Mina",
    },
    {
      imglink: "https://i.postimg.cc/4NhqByys/member-2.png",
      username: "jackson Aston",
    },
    {
      imglink: "https://i.postimg.cc/FH5qqvkc/member-3.png",
      username: "Samona Rose",
    },
    {
      imglink: "https://i.postimg.cc/Sx65bPcP/member-4.png",
      username: "Mike Pérez",
    },
  ];
  return (
    <>
      <div className="sidebar-title">
        <h4>Conversation</h4>
        <a href="/">Hide Chat</a>
      </div>
      {shortcut.map((data) => {
        return (
          <div className="online-list">
            <div className="online">
              <img src={data.imglink} alt={data.username} />
            </div>
            <p>{data.username}</p>
          </div>
        );
      })}
    </>
  );
};

export default RightSidebar;
