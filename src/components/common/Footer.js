/**
 * Functional component for creating the footer of the application
 * @returns JSX representing the footer of the application
 */
const Footer = () => {
  return (
    <div className="footer">
      <p>Copyright 2022 - Vkive Tutorials</p>
    </div>
  )
}

export default Footer